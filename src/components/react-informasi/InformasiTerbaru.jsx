import { useEffect, useState } from "react";
import { createClient } from "contentful";

export default function InformasiTerbaru() {
  const [items, setItems] = useState([]);

  const getItem = async () => {
    const item = await createClient({
      space: "t4ogu3wr0yrv",
      environment: "master",
      accessToken: "kNh7mQ055vH1CaHZmqGo6MgFjsSTYD74sQjBBVZE0cQ",
    }).getEntries({
      content_type: "beritaDanKegiatan",
      limit: 8,
      order: "-fields.tanggalTerbit",
    });
    console.log(item.items);
    setItems(item.items);
  };

  useEffect(() => {
    getItem();

    const onScroll = (e) => {
      const { scrollHeight } = e.target.scrollingElement;
      console.log(scrollHeight);
    };

    document.addEventListener("scroll", onScroll);
    return () => {
      document.removeEventListener("scroll", onScroll);
    };
  }, []);

  return (
    <div className="bg-white pt-10 lg:pt-20">
      <div className="max-w-screen-2xl px-4 md:px-8 mx-auto pb-10 lg:pb-20">
        <div className="grid sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4 md:gap-6 xl:gap-8">
          {items?.map((item) => (
            <a
              key={item.fields.slug}
              href={`https://lppntb.com/berita-dan-kegiatan/${item.fields.slug}`}
              className="group h-48 md:h-64 xl:h-96 flex flex-col bg-gray-100 rounded-lg shadow-lg overflow-hidden relative"
              target="_blank"
            >
              <div className="bg-slate-400 mix-blend-multiply absolute inset-0 z-10" />
              <img
                src={`https:${item.fields.cover.fields.file.url}`}
                loading="lazy"
                alt="Photo Agung Dwitatwa"
                className="w-full h-full object-cover object-center absolute inset-0 group-hover:scale-110 transition duration-200"
              />

              <div className="bg-gradient-to-t from-gray-800 md:via-transparent to-transparent absolute inset-0 pointer-events-none" />

              <div className="relative p-4 mt-auto z-20">
                <span className="flex items-center gap-2 text-gray-200 text-sm">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-6 h-6"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M6.75 3v2.25M17.25 3v2.25M3 18.75V7.5a2.25 2.25 0 012.25-2.25h13.5A2.25 2.25 0 0121 7.5v11.25m-18 0A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75m-18 0v-7.5A2.25 2.25 0 015.25 9h13.5A2.25 2.25 0 0121 11.25v7.5m-9-6h.008v.008H12v-.008zM12 15h.008v.008H12V15zm0 2.25h.008v.008H12v-.008zM9.75 15h.008v.008H9.75V15zm0 2.25h.008v.008H9.75v-.008zM7.5 15h.008v.008H7.5V15zm0 2.25h.008v.008H7.5v-.008zm6.75-4.5h.008v.008h-.008v-.008zm0 2.25h.008v.008h-.008V15zm0 2.25h.008v.008h-.008v-.008zm2.25-4.5h.008v.008H16.5v-.008zm0 2.25h.008v.008H16.5V15z"
                    />
                  </svg>
                  <p>
                    {item.fields.tanggalTerbit.split("-").reverse().join("-")}
                  </p>
                </span>
                <h2 className="text-white text-xl font-semibold transition duration-100 mb-2">
                  {item.fields?.judul.substr(0, 60) + "..."}
                </h2>

                <span className="text-blue-300 font-semibold">
                  Selengkapnya
                </span>
              </div>
            </a>
          ))}
        </div>
      </div>
    </div>
  );
}
