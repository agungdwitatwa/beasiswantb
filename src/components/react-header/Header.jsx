import { useState } from "react";
import Socmed from "./Socmed";

export default function Header() {
  const [showMenu, setShowMenu] = useState(false);
  const toggleButton = (status) => {
    if (status)
      return "inline-flex items-center lg:hidden bg-white-900 border border-blue-900 focus-visible:ring ring-blue-300 text-blue-900 active:text-blue-300 text-sm md:text-base font-semibold rounded-lg gap-2 px-2.5 py-2";
    return "inline-flex items-center lg:hidden bg-blue-900 border border-blue-900 focus-visible:ring ring-blue-300 text-white active:text-blue-300 text-sm md:text-base font-semibold rounded-lg gap-2 px-2.5 py-2";
  };

  return (
    <div className="relative ">
      <header className="flex justify-between items-center py-4 md:py-8">
        <a
          href="/"
          className="inline-flex items-center text-slate-700 text-2xl md:text-3xl font-bold gap-2.5"
          aria-label="logo"
        >
          <img src="/logo.png" alt="logo-pemprov-ntb" className="w-6" />
          Beasiswa NTB
        </a>

        <nav className="hidden lg:flex gap-12">
          <a
            href="/informasi"
            className="text-gray-600 hover:text-blue-500 active:text-blue-700 text-lg font-semibold transition duration-100"
          >
            Informasi Terbaru
          </a>
          <a
            href="/jenis-beasiswa"
            className="text-gray-600 hover:text-blue-500 active:text-blue-700 text-lg font-semibold transition duration-100"
          >
            Jenis Beasiswa
          </a>
          <a
            href="/persyaratan-umum"
            className="text-gray-600 hover:text-blue-500 active:text-blue-700 text-lg font-semibold transition duration-100"
          >
            Persyaratan Umum
          </a>
        </nav>
        <Socmed />
        <button
          type="button"
          className={toggleButton(showMenu)}
          onClick={() => setShowMenu(!showMenu)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-6 w-6"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h6a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
              clipRule="evenodd"
            ></path>
          </svg>
          {showMenu ? "Close" : "Menu"}
        </button>
      </header>
      {showMenu && (
        <div className="flex flex-col gap-2 mb-3">
          <a
            href="/informasi"
            className="border-2 border-blue-900 rounded-md p-3 text-blue-900 text-center"
          >
            Informasi Terbaru
          </a>
          <a
            href="/jenis-beasiswa"
            className="border-2 border-blue-900 rounded-md p-3 text-blue-900 text-center"
          >
            Jenis Beasiswa
          </a>
          <a
            href="/persyaratan-umum"
            className="border-2 border-blue-900 rounded-md p-3 text-blue-900 text-center"
          >
            Persyaratan Umum
          </a>
        </div>
      )}
    </div>
  );
}
